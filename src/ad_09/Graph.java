/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad_09;

import java.util.HashMap;

/**
 *
 * @author abp514
 */
public class Graph implements IGraph{
    Node nodes[];
    int[][] matrix;
 
    
    public Graph(Node nodes[]){
        this.nodes = nodes;
        this.matrix = new int[nodes.length][nodes.length];
        createGraph();
    }

    
        
    @Override
    public void createGraph() {
        
        for(int i=0; i<nodes.length; i++){
            for(int j=0; j<i; j++){
                int costs = 1+(int)(Math.random()*8);
                int nodeIndex = (int)(Math.random()*nodes.length);
                if (nodeIndex != i){
                    nodes[i].neighbours.putIfAbsent(nodes[nodeIndex], costs);
                    nodes[nodeIndex].neighbours.putIfAbsent(nodes[i], costs);
                    if (matrix[i][nodeIndex] == 0)
                    {
                        matrix[i][nodeIndex] = costs;
                        matrix[nodeIndex][i] = matrix[i][nodeIndex];
                    }
                }
            }
        }
    }
    
    
    
    @Override
    public void insertNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node[] getNeighbours(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getGewichtung(Node node1, Node node2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNeighbour(Node node1, Node node2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node[] getNodes() {
        return nodes;
    }
    
    public int[][] getMatrix(){
        return matrix;
    }

}
