/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad_09;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class AdjacencyList implements IGraph{
    Node[] nodes;

    public AdjacencyList(){
        this(new Node[0]);
    }

    AdjacencyList(Node[] nodes){
        this.nodes = nodes;
        createGraph();
    }
    
    AdjacencyList(Graph graph){
        this.nodes = graph.getNodes();
    }

    @Override
    public void createGraph() {
        for(int i=0; i<nodes.length; i++){
            for(int j=0; j<nodes.length; j++){
                int costs = 1+(int)(Math.random()*8);
                int nodeIndex = (int)(Math.random()*nodes.length);
                if (nodeIndex != i){
                    nodes[i].neighbours.putIfAbsent(nodes[nodeIndex], costs);
                    nodes[nodeIndex].neighbours.putIfAbsent(nodes[i], costs);
                }
            }
        }
    }


    @Override
    public void insertNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    @Override
    public Node[] getNeighbours(Node node) {
        return node.neighbours.keySet().toArray(new Node[0]);
    }



    @Override
    public boolean deleteNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    @Override
    public int getGewichtung(Node node1, Node node2) {
        return node1.neighbours.get(node2);
    }



    @Override
    public boolean isNeighbour(Node node1, Node node2) {
        return node1.neighbours.containsKey(node2);
    }

    @Override
    public Node[] getNodes() {
        return nodes;
    }

    void printMatrix(){
        int arr[][] = new int[nodes.length][nodes.length];

        for (int i=0; i<nodes.length; i++){
            for (Entry<Node,Integer> entry : nodes[i].neighbours.entrySet()){
                arr[i][Arrays.asList(nodes).indexOf(entry.getKey())] = entry.getValue();
            }
        }
        System.out.print("    ");
        for(int j=0; j<nodes.length; j++){
            System.out.print(" ");
            System.out.print(j);
            System.out.print(" ");
        }
        System.out.println(" ");

        for(int j=0; j<nodes.length; j++)
        {
            System.out.print(j+"   ");
            for(int i = 0; i<nodes.length; i++)
            {
                System.out.print("|");
                System.out.print(arr[j][i]);
                System.out.print("|");

            }
            System.out.println("");
        }
    }

    void printDistance()
    {
        System.out.println("Distanzen zu destination " + Arrays.asList(nodes).indexOf(nodes[0].destination) + ": ");
        for(int i = 0; i < nodes.length; i++)
        {
            if(nodes[i] != null)
                System.out.print("("+Arrays.asList(nodes).indexOf(nodes[i])+"|"+nodes[i].distance+")" + " ");
        }
        System.out.println("");
    }

    void printWayToDestination(Node fromNode){
        System.out.println("Print way to destination from:" + Arrays.asList(nodes).indexOf(fromNode));
        Node node = fromNode;
        while (node != node.destination){
            System.out.print(Arrays.asList(nodes).indexOf(node) + " ");
            node = node.nextNodeShortestWay;
        }
        System.out.println();
    }

    void printNeighbours(Node node){

        System.out.print("Nachbarn von " + Arrays.asList(nodes).indexOf(node) + ": ");
        for(Entry<Node,Integer> entry : node.neighbours.entrySet())
        {
            System.out.print(Arrays.asList(nodes).indexOf(entry.getKey())+ " ");
        }
        System.out.println("");
    }
}
