package ad_09;

import java.util.Arrays;

public class DijkstraTiming extends Dijkstra {
	
	DijkstraTiming(){
            super();
	}
	
	public DijkstraTiming(Graph graph){
            super(graph);
        }
        
        public DijkstraTiming(AdjacencyMatrix matrix){
            super(matrix);
        }
        
        public DijkstraTiming(AdjacencyList list){
            super(list);
        }

	long dijkstraAlgorithmTiming(Node beginNode){
		long timeStart 	= System.nanoTime();
		dijkstraAlgorithm(beginNode);
		long timeEnd 	= System.nanoTime();
		
		return timeEnd - timeStart;
	}
	
	long evaluatedNeighboursTiming(Node evaluationNode){
		long timeStart 	= System.nanoTime();
		evaluatedNeighbours(evaluationNode);
		long timeEnd 	= System.nanoTime();
		
		return timeEnd - timeStart;
    }

    long notInSettledNodesTiming(Node node){
    	long timeStart 	= System.nanoTime();
    	notInSettledNodes(node);
		long timeEnd 	= System.nanoTime();
		
		return timeEnd - timeStart;
    }

    long getNodeWithLowestDistanceTiming(Node[] nodes){
    	long timeStart 	= System.nanoTime();
    	getNodeWithLowestDistance(nodes);
		long timeEnd 	= System.nanoTime();
		
		return timeEnd - timeStart;
    }

    long notEmptyTiming(Node[] nodes){
    	long timeStart 	= System.nanoTime();
    	notEmpty(nodes);
		long timeEnd 	= System.nanoTime();
		
		return timeEnd - timeStart;
    }
}
