/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad_09;

import java.util.Arrays;


public class Dijkstra {

    IGraph graph;
    Node[] nodes;
    Node[] settledNodes;
    Node[] unsettledNodes;


    public Dijkstra(){
        super();
    }

    public Dijkstra(IGraph graph)
    {
        this.graph = graph;
        nodes = graph.getNodes();
    }

    public void dijkstraAlgorithm(Node beginNode){

        for (int i=0; i<nodes.length; i++){
            nodes[i].distance = Integer.MAX_VALUE;
            nodes[i].destination = beginNode;
        }

        settledNodes = new Node[nodes.length];
        unsettledNodes = new Node[nodes.length];

        unsettledNodes[Arrays.asList(nodes).indexOf(beginNode)] = beginNode;

        beginNode.distance = 0;

        while(notEmpty(unsettledNodes)){
            Node evaluationNode = getNodeWithLowestDistance(unsettledNodes);
            int  evaluationNodeIndex = Arrays.asList(nodes).indexOf(evaluationNode);
            unsettledNodes[evaluationNodeIndex] = null;
            settledNodes[evaluationNodeIndex] = evaluationNode;
            evaluatedNeighbours(evaluationNode);
        }
    }

    void evaluatedNeighbours(Node evaluationNode){
        for(Node destinationNode : graph.getNeighbours(evaluationNode)){
            if(destinationNode != null && notInSettledNodes(destinationNode)){
                int edgeDistance = graph.getGewichtung(evaluationNode, destinationNode);
                int newDistance = evaluationNode.distance + edgeDistance;

                if(destinationNode.distance > newDistance){
                    destinationNode.distance = newDistance;
                    destinationNode.nextNodeShortestWay = evaluationNode;
                    unsettledNodes[Arrays.asList(nodes).indexOf(destinationNode)] = destinationNode;
                }
            }
        }
    }

    boolean notInSettledNodes(Node node){
        for(int i=0; i<settledNodes.length; i++){
            if(settledNodes[i] == node)
                return false;
        }

        return true;
    }

    Node getNodeWithLowestDistance(Node[] nodes){
        Node lowest = null;

        for(int i=0; i<nodes.length; i++){
            if(nodes[i] != null){
                lowest = nodes[i];
                break;
            }
        }
        if (lowest == null)
            return null;

        for(int i=0; i<nodes.length; i++){
            if(nodes[i] != null){
                if(nodes[i].distance < lowest.distance)
                    lowest = nodes[i];
            }
        }
        return lowest;
    }

    boolean notEmpty(Node[] nodes){
        for(int i=0; i<nodes.length; i++){
            if(nodes[i] != null)
                return true;
        }
        return false;
    }
}
