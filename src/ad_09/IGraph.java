/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad_09;

public interface IGraph{

    void createGraph();

    void insertNode(Node node);

    Node[] getNeighbours(Node node);

    boolean deleteNode(Node node);

    int getGewichtung(Node node1, Node node2);

    boolean isNeighbour(Node node1, Node node2);

    Node[] getNodes();
}
