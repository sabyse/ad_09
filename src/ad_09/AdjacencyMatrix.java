/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad_09;
import java.util.Arrays;

/**
 *
 * @author Admin
 */
public class AdjacencyMatrix implements IGraph{

    int [][] matrix;
    Node[] nodes;

    AdjacencyMatrix(){
        this(new Node[0]);
    }

    AdjacencyMatrix(Node[] node){
        this.matrix = new int[node.length][node.length];
        this.nodes = node;
        createGraph();
    }
    
    AdjacencyMatrix(Graph graph){
        this.matrix = graph.getMatrix();
        this.nodes = graph.getNodes();
    }

    @Override
    public void createGraph() {
        int i = 0;
        int j = 0;
        for(j=0; j <nodes.length; j++)
        {
            for(i = 0; i<=j;i++)
            {
                matrix[j][i] = (int)(Math.random()*9);
                matrix[i][j] = matrix[j][i];
                if(i==j)
                    matrix[i][j]=0;
            }
        }
    }

    @Override
    public void insertNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node[] getNeighbours(Node node) {
        Node[] neighbourArr = new Node[nodes.length];
        int index = Arrays.asList(nodes).indexOf(node);
        for(int i = 0; i < nodes.length; i++){
            if(matrix[i][index] > 0)
            {
                neighbourArr[i]= nodes[i];
            }
        }
        return neighbourArr;
    }

    @Override
    public boolean deleteNode(Node node) {
        int index = Arrays.asList(nodes).indexOf(node);
        if(nodes[index]!=null)
        {
            nodes[index]=null;
            for(int i = 0; i <nodes.length; i++)
            {
                matrix[i][index] = 0;
                matrix[index][i] = 0;
            }
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int getGewichtung(Node node1, Node node2) {
        int index1 = Arrays.asList(nodes).indexOf(node1);
        int index2 = Arrays.asList(nodes).indexOf(node2);
        return matrix[index1][index2];
    }

    @Override
    public boolean isNeighbour(Node node1, Node node2) {
        int index1 = Arrays.asList(nodes).indexOf(node1);
        int index2 = Arrays.asList(nodes).indexOf(node2);

        return matrix[index1][index2] > 0;
    }


    void printMatrix(){
        System.out.print("    ");
        for(int j=0; j<nodes.length; j++){
            System.out.print(" ");
            System.out.print(j);
            System.out.print(" ");
        }
        System.out.println(" ");

        for(int j=0; j<nodes.length; j++)
        {
            System.out.print(j+"   ");
            for(int i = 0; i<nodes.length; i++)
            {
                System.out.print("|");
                System.out.print(matrix[j][i]);
                System.out.print("|");

            }
            System.out.println("");
        }
    }

    void printDistance()
    {
        System.out.println("Distanzen zu destination " + Arrays.asList(nodes).indexOf(nodes[0].destination) + ": ");
        for(int i = 0; i < nodes.length; i++)
        {
            if(nodes[i] != null)
                System.out.print("("+Arrays.asList(nodes).indexOf(nodes[i])+"|"+nodes[i].distance+")" + " ");
        }
        System.out.println("");
    }

    void printWayToDestination(Node fromNode){
        System.out.println("Print way to destination from:" + Arrays.asList(nodes).indexOf(fromNode));
        Node node = fromNode;
        while (node != node.destination){
            System.out.print(Arrays.asList(nodes).indexOf(node) + " ");
            node = node.nextNodeShortestWay;
        }
        System.out.println();
    }

    void printNeighbours(Node node){
        Node[] neighbours;

        System.out.print("Nachbarn von " + Arrays.asList(nodes).indexOf(node) + ": ");
        neighbours = getNeighbours(node);
        for(int i = 0; i < nodes.length; i++)
        {
            if(neighbours[i] != null)
                System.out.print(i+ " ");
        }
        System.out.println("");
    }

    @Override
    public Node[] getNodes(){
        return nodes;
    }





}
