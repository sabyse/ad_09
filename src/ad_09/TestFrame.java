package ad_09;

import java.util.HashMap;


public class TestFrame {

    public static void main(String[] args) {

        Node[] nodes = new Node[7];
        for (int i=0; i<nodes.length; i++){
            nodes[i] = new Node();
            nodes[i].neighbours = new HashMap<Node,Integer>();
        }

        Graph graph = new Graph(nodes);
        AdjacencyList list = new AdjacencyList(graph);
        AdjacencyMatrix matrix = new AdjacencyMatrix(graph);
//        AdjacencyList graph = new AdjacencyList(nodes);

        System.out.println("");

        matrix.printMatrix();
        list.printMatrix();
        
        System.out.println("");
        
        for (int i=0; i<list.nodes.length; i++){
            list.printNeighbours(list.nodes[i]);
            matrix.printNeighbours(matrix.nodes[i]);
        }

        DijkstraTiming dijkstraListTiming = new DijkstraTiming(list);
        DijkstraTiming dijkstraMatrixTiming = new DijkstraTiming(matrix);
        
        System.out.println("");
        

        for (int i=0; i<nodes.length; i++){
            System.out.println("TIME for dikstraAlgorithm for Node " + i + " on list in Nanoseconds:   " + dijkstraListTiming.dijkstraAlgorithmTiming(list.nodes[i]));
            System.out.println("TIME for dikstraAlgorithm for Node " + i + " on matrix in Nanoseconds: " + dijkstraMatrixTiming.dijkstraAlgorithmTiming(matrix.nodes[i]));

        }
        System.out.println("");
        
        Dijkstra test = new Dijkstra(list);
        Dijkstra test2 = new Dijkstra(matrix);
        
        test2.dijkstraAlgorithm(matrix.nodes[0]);
        test.dijkstraAlgorithm(list.nodes[0]);

        list.printDistance();
        matrix.printDistance();
        
        
        System.out.println("");


        for (int i=1; i<list.nodes.length; i++){
            list.printWayToDestination(list.nodes[i]);
            matrix.printWayToDestination(matrix.nodes[i]);
        }
    
        System.out.println("");


    }

}

